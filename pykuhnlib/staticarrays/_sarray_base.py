"""
Main implementation file for staticarrays
"""
import numpy as np

class StaticArray(np.ndarray):
    """Convenience base class for fixed-size arrays derived from :py:class:`numpy.ndarray`

    ``StaticArray`` should not be used directly. Rather it should be subclassed with
    the subclass defining a class attribute ``_shape`` to set the size of newly created
    ``StaticArray`` subclass.

    For example::

        class Vector3(StaticArray):
            _shape = (3,)
            x = IndexDescriptor(0)
            y = IndexDescriptor(1)
            z = IndexDescriptor(2)

        class Matrix3x3(StaticArray):
            _shape = (3,3)

        avec = Vector3([1, 2, 3], dtype=int)
        amat = Matrix3x3([[1, 2, 3], [4, 5, 6], [7, 8, 9]], dtype=float)
        avec.x == avec[0]

    ..  seealso::
        `Subclassing ndarray <https://docs.scipy.org/doc/numpy/user/basics.subclassing.html>`_

    """
    # Subclasses *must* define the _shape class attribute
    _shape = (0,)
    def __new__(cls, value=None, dtype=None):
        """Create a new StaticArray object.

        Returns a :py:meth:`numpy.ndarray.view` cast of the newly created :py:class:`numpy.ndarray`

        Args:
            value (List[Number], optional): Defaults to None. Initial array contents
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            StaticArray: a view of the array as the derived class
        """
        if value is None:
            obj = np.zeros(cls._shape, dtype=dtype)
        else:
            obj = value
            if not isinstance(value, np.ndarray):
                obj = np.array(value, dtype=dtype)
        obj.shape = cls._shape
        obj = obj.view(cls)
        return obj

    @classmethod
    def create_eye(cls, index=0, dtype=None):
        """Create a fixed-size identity matrix or vector

        Creates unit vectors for one-dimensional array type subclasses (``_shape=(N,)``).

        Creates identity matrices for two-dimensional matrix type subclasses (``_shape=(N,N)``)

        Args:
            index (int, optional): Defaults to 0. For one-dimensional arrays
                Index of the element to set to one. Not used for two-dimensional matrices
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            StaticArray derived class: a view of the new identity matrix or vector
        """

        if len(cls._shape) == 1:
            # create a unit vector using index
            obj = cls(dtype=dtype)
            obj[index] = 1
            return obj
        else:
            # create an identity matrix
            return cls(np.eye(cls._shape[0], dtype=dtype))

class IndexDescriptor(object):
    """ Helper class to provide indexed get/set access properties for classes with an
    array access method ``[]``.

    For example, a two-dimensional array can be accessed using the ``.x`` or ``.y`` properties::

        class Vector2(StaticArray):
            _shape = (2,)
            x = IndexDescriptor(0)
            y = IndexDescriptor(1)

        avec = Vector2([1, 2], dtype=int)
        avec.x == avec[0]
    """

    def __init__(self, index):
        self._index = index
    def __get__(self, obj, target):
        return obj[self._index]
    def __set__(self, obj, value):
        obj[self._index] = value
