"""
Example :py:mod:`pykuhnlib.staticarray` subclasses for fixed sized vectors and matrices
"""
from . import StaticArray, IndexDescriptor

__all__ = [
    "Vec3",
    "Mat33",
]

class Vec3(StaticArray):
    """ Example 3-D static vector
    """

    _shape = (3,)
    x = IndexDescriptor(0)
    """x-component property"""
    y = IndexDescriptor(1)
    """y-component property"""
    z = IndexDescriptor(2)
    """z-component property"""
    xy = IndexDescriptor([0, 1])
    """xy-components array property"""
    xz = IndexDescriptor([0, 2])
    """xz-components array property"""
    yz = IndexDescriptor([1, 2])
    """yz-components array property"""

    create_ux = classmethod(lambda cls, dtype=None: cls.create_eye(0, dtype=dtype))
    """Create a unit x-vector
    """

    create_uy = classmethod(lambda cls, dtype=None: cls.create_eye(1, dtype=dtype))
    """Create a unit y-vector
    """

    create_uz = classmethod(lambda cls, dtype=None: cls.create_eye(2, dtype=dtype))
    """Create a unit z-vector
    """

class Mat33(StaticArray):
    """ Example 3x3 static matrix"""
    _shape = (3, 3)
