"""
Fixed size arrays as subclasses of :py:class:`numpy.ndarray`
"""

from ._sarray_base import *

__all__ = [
    "StaticArray",
    "IndexDescriptor",
]
