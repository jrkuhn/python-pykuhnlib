import unittest
import numpy as np
import pykuhnlib.staticarrays as sa
import pykuhnlib.staticarrays.vecmat as sav


class TempVec3(sa.StaticArray):
    _shape = (3,)
    x = sa.IndexDescriptor(0)
    y = sa.IndexDescriptor(1)
    z = sa.IndexDescriptor(2)

class TestStaticArray(unittest.TestCase):

    def test_create(self):
        a = TempVec3([1, 2, 3],dtype=int)
        self.assertEqual(type(a),TempVec3)
        self.assertEqual(a.x, 1)
        self.assertEqual(a.y, 2)
        self.assertEqual(a.z, 3)

class TestVecMat(unittest.TestCase):
    def test_vec3_create(self):
        a = sav.Vec3([1, 2, 3],dtype=int)
        self.assertTrue(np.allclose(a, [1, 2, 3]), msg="Vec3")
