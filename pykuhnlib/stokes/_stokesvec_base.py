"""
Main implementation file for Stokes vectors
"""
import numpy as np
from numpy import linalg as LA
from pykuhnlib import staticarrays as sa

class StokesVec4(sa.StaticArray):
    """Standard 4-element Stokes vector

    Fixed-size array holding a Stokes vector :math:`(s_0,s_1,s_2,s_3)^{-1}`
    """

    _shape = (4,)
    # Give Stokes vectors higher output priority than Mueller matrices during
    # matrix math
    __array_priority__ = 1.0

    s0 = sa.IndexDescriptor(0)
    """:math:`s_0` property"""
    s1 = sa.IndexDescriptor(1)
    """:math:`s_1` property"""
    s2 = sa.IndexDescriptor(2)
    """:math:`s_2` property"""
    s3 = sa.IndexDescriptor(3)
    """:math:`s_3` property"""
    s1s2 = sa.IndexDescriptor([1, 2])
    """:math:`s_{12}` property"""
    s1s2s3 = sa.IndexDescriptor([1, 2, 3])
    """:math:`s_{123}` property"""
    create_us0 = classmethod(lambda cls, dtype=None: cls.create_eye(0, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_0` (unpolarized)"""
    create_us1 = classmethod(lambda cls, dtype=None: cls.create_eye(1, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_1`"""
    create_us2 = classmethod(lambda cls, dtype=None: cls.create_eye(2, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_2`"""
    create_us3 = classmethod(lambda cls, dtype=None: cls.create_eye(2, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_3`"""

    @classmethod
    def create_from_ax3(cls, s0, saxis, dtype=None):
        """Create a 4-element Stokes vector from a 3-element Stokes axis

        Args:
            s0 (Number): :math:`s_0` component representing the flux
            saxis (StokesAx3): 3-element stokes axis direction on the Poincare sphere
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            StokesVec4: A 4-element Stokes vector
        """

        return cls(np.append(s0, saxis), dtype=dtype)
    def flux(self):
        """Total intensity of a Stokes vector"""
        return self.s0
    def dop(self):
        """Degree of polarization of a Stokes vector"""
        return 0 if self.s0 == 0 else LA.norm(self.s1s2s3) / self.s0
    def dolp(self):
        """Degree of linear polarization of a Stokes vector"""
        return 0 if self.s0 == 0 else LA.norm(self.s1s2) / self.s0
    def docp(self):
        """Degree of circular polarization of a Stokes vector"""
        return 0 if self.s0 == 0 else self.s3 / self.s0
    def ellipticity(self):
        """Ratio of semiminor to semimajor axes of the ellipse.

        Varies from 0 for linearly polarized light to 1 for circularly polarized light.
        """
        d = self.s0 + LA.norm(self.s1s2)
        return 0 if d == 0 else self.s3 / d
    def major_axis(self):
        """Length of the semi-major axis of the polarization ellipse"""
        lp = LA.norm(self.s1s2)
        ip = LA.norm(self.s1s2s3)
        return np.sqrt((ip + lp)/2)
    def minor_axis(self):
        """Length of the semi-minor axis of the polarization ellipse"""
        lp = LA.norm(self.s1s2)
        ip = LA.norm(self.s1s2s3)
        return np.sqrt((ip-lp)/2)
    def major_angle(self):
        """Orientation angle of the major axis (psi)"""
        return np.arctan2(self.s2, self.s1) / 2
    def ellipticity_angle(self):
        """Angles subtended by the relative lengths of the major to
        the minor axis :math:`\\chi` (chi).

        Varies from zero for linear polarization (minor=0) to :math:`\\pi/4`
        (45º) for circular polarization (major==minor).
        Positive for right-circular, negative for left-circular.
        """
        return np.sign(self.s3) * np.arctan2(self.minor_axis(), self.major_axis())
    def eccentricity(self):
        """Eccentricity of the polarization ellipse.

        Zero for circularly polarized light, increases as the
        ellipse becomes thinner (more cigar-shaped), and becomes
        one for linearly polarized light.
        """
        return np.sqrt(1.0 - self.ellipticity() ** 2)
    def stokes_sphere_angles(self):
        """returns a tuple (:math:`\\phi_{az}`, :math:`\\theta_{inc}`), the azimuthal
        and inclination angles around the Poincare Sphere.

        This pair can be used for a 2D "cylindrical" plot of the Poincare sphere
        with [x,y] = [phi_az, theta_inc] representing the equator and poles of
        the poincare axis angles.
        """
        phi = 2*self.major_angle()
        theta = 2*self.ellipticity_angle()
        return phi, theta

class StokesAx3(sa.StaticArray):
    """Non-standard 3-element Stokes axis containing only the polarized compoenets
    of the Stokes vector

    Fixed-size array holding a Stokes sub-vector :math:`(s_1,s_2,s_3)^{-1}`
    """
    _shape = (3,)
    # Give Stokes axis vectors higher output priority than 3x3 Mueller matrices during
    # matrix math

    __array_priority__ = 1.0
    s1 = sa.IndexDescriptor(0)
    """:math:`s_1` property"""
    s2 = sa.IndexDescriptor(1)
    """:math:`s_2` property"""
    s3 = sa.IndexDescriptor(2)
    """:math:`s_3` property"""
    s1s2 = sa.IndexDescriptor([0, 1])
    """:math:`s_{12}` property"""
    s1s2s3 = sa.IndexDescriptor([0, 1, 2])
    """:math:`s_{123}` property"""
    create_us1 = classmethod(lambda cls, dtype=None: cls.create_eye(0, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_1` axis"""
    create_us2 = classmethod(lambda cls, dtype=None: cls.create_eye(1, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_2` axis`"""
    create_us3 = classmethod(lambda cls, dtype=None: cls.create_eye(2, dtype=dtype))
    """Create a unit Stokes vector along :math:`s_3` axis"""

    @classmethod
    def create_from_vec4(cls, svec, dtype=None):
        """Create a 3-element Stokes axis from a 4-element Stokes vector

        Args:
            svec (StokesVec4): 4-element stokes vector
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            StokesAx3: A 3-element Stokes axis
        """

        return cls(svec.s1s2s3, dtype=dtype)
    def major_axis(self):
        """Length of the semi-major axis of the polarization ellipse"""
        lp = LA.norm(self.s1s2)
        ip = LA.norm(self.s1s2s3)
        return np.sqrt((ip + lp)/2)
    def minor_axis(self):
        """Length of the semi-minor axis of the polarization ellipse"""
        lp = LA.norm(self.s1s2)
        ip = LA.norm(self.s1s2s3)
        return np.sqrt((ip-lp)/2)
    def major_angle(self):
        """Orientation angle of the major axis (psi)"""
        return np.arctan2(self.s2, self.s1) / 2
    def ellipticity_angle(self):
        """Angles subtended by the relative lengths of the major to
        the minor axis :math:`\\chi` (chi).

        Varies from zero for linear polarization (minor=0) to :math:`\\pi/4`
        (45º) for circular polarization (major==minor).
        Positive for right-circular, negative for left-circular.
        """
        return np.sign(self.s3) * np.arctan2(self.minor_axis(), self.major_axis())
    def eccentricity(self):
        """Eccentricity of the polarization ellipse.

        Zero for circularly polarized light, increases as the
        ellipse becomes thinner (more cigar-shaped), and becomes
        one for linearly polarized light.
        """
        return np.sqrt(1.0 - self.ellipticity() ** 2)
    def stokes_sphere_angles(self):
        """returns a tuple (:math:`\\phi_{az}`, :math:`\\theta_{inc}`), the azimuthal
        and inclination angles around the Poincare Sphere.

        This pair can be used for a 2D "cylindrical" plot of the Poincare sphere
        with [x,y] = [phi_az, theta_inc] representing the equator and poles of
        the poincare axis angles.
        """
        phi = 2*self.major_angle()
        theta = 2*self.ellipticity_angle()
        return phi, theta

class PolEllipse4(sa.StaticArray):
    """A 4-element array representing a Polarization Ellipse.

    Fixed-size array holding information about the polarization ellipse
    as :math:`(a,b,\\psi,\\chi)^{-1}`

    Args:
        a (Number):     major axis length
        b (Number):     minor axis length
        psi (Number):   orientation angle :math:`\\psi`
                        between the major axis of the ellipse and the x-axis
        chi (Number):   ellipticity angle :math:`\\chi` defined as
                        :math:`\\tan^{-1}\\frac{b}{a}`

    ..  seealso:: :doc:`about_spherical_coords` and the `Wikipedia section on
        the Polarization ellipse
        <https://en.wikipedia.org/wiki/Polarization_(waves)#Polarization_ellipse>`_
    """

    _shape = (4,)
    a = sa.IndexDescriptor(0)
    """major axis length property"""
    b = sa.IndexDescriptor(1)
    """minor axis length property"""
    psi = sa.IndexDescriptor(2)
    """orientation angle :math:`\\psi` between the major axis of the ellipse and the x-axis"""
    chi = sa.IndexDescriptor(3)
    """ellipticity angle :math:`\\chi` defined as :math:`\\tan^{-1}\\frac{b}{a}`"""
    @classmethod
    def create_from_ax3(cls, saxis, dtype=None):
        """Create a polarization ellipse from a 3-element Stokes axis.

        Args:
            saxis (StokesAx3): 3-element stokes axis
            dtype ([type], optional): Defaults to None. [description]

        Returns:
            PolEllipse4: the polarization ellipse
        """
        return cls([
            saxis.major_axis,
            saxis.minor_axis,
            saxis.major_angle,
            saxis.ellipticity_angle
            ], dtype=dtype)

    @classmethod
    def create_from_vec4(cls, svec, dtype=None):
        """Create a polarization ellipse from a 4-element Stokes vector.

        Args:
            svec (StokesVec4): 4-element stokes vector
            dtype ([type], optional): Defaults to None. [description]

        Returns:
            PolEllipse4: the polarization ellipse
        """
        return cls([
            svec.major_axis,
            svec.minor_axis,
            svec.major_angle,
            svec.ellipticity_angle
            ], dtype=dtype)
