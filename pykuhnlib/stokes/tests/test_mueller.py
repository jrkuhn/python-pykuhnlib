import unittest
import numpy as np
from pprint import pprint, pformat
import pykuhnlib.stokes as stokes
from pykuhnlib.stokes.mueller import *
from pykuhnlib.stokes.stokesvecex import *

class TestMuellerMat44(unittest.TestCase):

    # def test_rot_debug(self):
    #     M1 = MuellerMat44.create_rot(np.pi/2)
    #     # print('\n======== testing rot.\nM = {}'.format(np.array_repr(M, precision=2, suppress_small=True)))
    #     sin = SLH.vec
    #     # print('========= sin = {}'.format(np.array_repr(sin, precision=2, suppress_small=True)))
    #     # print('========= CALLING DOT PRODUCT ===========')
    #     # sout = np.dot(M, sin)
    #     # sout = M.dot(sin)
    #     sout = M1 @ sin
    #     # print('======== sout = {}'.format(np.array_repr(sout, precision=2, suppress_small=True)))
    #     self.assertTrue(np.allclose(sout, SLV.vec))

    def test_h_polar(self):
        M = MuellerMat44.create_h_polar()
        sout = M @ SLH.vec
        self.assertEqual(type(sout), StokesVec4, msg='type')
        self.assertTrue(np.allclose(sout, SLH.vec), msg='H @ h')
        sout = M @ SLV.vec
        self.assertTrue(np.allclose(sout, SZERO.vec), msg='H @ v')

    def test_v_polar(self):
        M = MuellerMat44.create_v_polar()
        sout = M @ SLV.vec
        self.assertEqual(type(sout), StokesVec4, msg='type')
        self.assertTrue(np.allclose(sout, SLV.vec), msg='V @ v')
        sout = M @ SLH.vec
        self.assertTrue(np.allclose(sout, SZERO.vec), msg='V @ h')

    def test_rot(self):
        sout = MuellerMat44.create_rot(np.pi/4) @ SLH.vec
        self.assertEqual(type(sout), StokesVec4, msg='type')
        self.assertTrue(np.allclose(sout, SL45.vec), msg='rot 45 deg')

        sout = MuellerMat44.create_rot(np.pi/2) @ SLH.vec
        self.assertTrue(np.allclose(sout, SLV.vec), 'rot 90 deg')

        sout = MuellerMat44.create_rot(3*np.pi/4) @ SLH.vec
        self.assertTrue(np.allclose(sout, SL135.vec), 'rot 135 deg')

        sout = MuellerMat44.create_rot(np.pi) @ SLH.vec
        self.assertTrue(np.allclose(sout, SLH.vec), 'rot 180 deg')
