import unittest
import numpy as np
from pprint import pprint, pformat
import pykuhnlib.stokes as stokes
from pykuhnlib.stokes.stokesvecex import *


class TestStokesVec4(unittest.TestCase):

    def test_stokesvectoax(self):
        for ex in STOKES_EXAMPLES:
            sv = ex.vec
            sax = stokes.StokesAx3.create_from_vec4(sv)
            self.assertTrue(np.allclose(sv[1:4], sax), msg=ex.name)

    def test_flux(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.flux(), ex.flux, msg=ex.name)

    def test_dop(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.dop(), ex.dop, msg=ex.name)

    def test_dolp(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.dolp(), ex.dolp, msg=ex.name)

    def test_docp(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.docp(), ex.docp, msg=ex.name)

    def test_ellipticity(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.ellipticity(), ex.ellipticity, msg=ex.name)

    def test_major_axis(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.major_axis(), ex.major_axis, msg=ex.name)

    def test_minor_axis(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.minor_axis(), ex.minor_axis, msg=ex.name)

    def test_major_angle(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.major_angle(), ex.major_angle, msg=ex.name)

    def test_ellipticity_angle(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.ellipticity_angle(), ex.ellipticity_angle, msg=ex.name)

    def test_eccentricity(self):
        for ex in STOKES_EXAMPLES:
            self.assertAlmostEquals(ex.vec.eccentricity(), ex.eccentricity, msg=ex.name)

    def test_stokes_sphere_angles(self):
        for ex in STOKES_EXAMPLES:
            self.assertTrue(np.allclose(ex.vec.stokes_sphere_angles(), ex.stokes_sphere_angles), msg=ex.name)
