"""
Mueller matrix methods

Most of these methods were taken from the Handbook of Optics chapter on Polarization:

*Chipman, R.A. (1994). Polarimetry. In Handbook of Optics, (Handbook of optics).*
"""
import numpy as np
from numpy import linalg as LA
from pykuhnlib import staticarrays as sa
from pykuhnlib import stokes

class MuellerMat44(sa.StaticArray):
    _shape = (4,4)
    """Standard 4x4-element Mueller polarization matrix

    Fixed-size array holding a 4x4 Mueller matrix
    """
    @classmethod
    def create_rot(cls, psi, dtype=None):
        """Creates a Mueller rotation matrix to rotate about the optical axis
        by an angle :math:`\\psi`.

        ..  math::
            M_{R}(\\psi) = \\begin{pmatrix}
                1   &   0               &   0               &   0 \\\\
                0   &   \\cos 2\\psi    &  -\\sin 2\\psi    &   0 \\\\
                0   &   \\sin 2\\psi    &   \\cos 2\\psi    &   0 \\\\
                0   &   0               &   0               &   1
            \\end{pmatrix}

        Args:
            psi (Number): rotation angle in radians
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        cos2psi = np.cos(2*psi)
        sin2psi = np.sin(2*psi)
        return cls([
            [1, 0,          0,              0],
            [0, cos2psi,    -sin2psi,       0],
            [0, sin2psi,    cos2psi,        0],
            [0, 0,          0,              1]
            ], dtype=dtype)

    def rotated(self, psi, dtype=None):
        """Rotates a Mueller matrix about the optical axis
        by an angle :math:`\\psi`.

        Performs a versor rotation of a Mueller matrix :math:`M` by
        sandwiching it between two rotation matrices.

        ..  math::
            M_{\\psi} = M_R(\\psi) \\cdot M \\cdot M_R(-\\psi)

        Args:
            psi (Number): rotation angle in radians
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A rotated copy of a 4x4 Mueller matrix
        """
        MRp = MuellerMat44.create_rot(psi)
        MRn = MuellerMat44.create_rot(-pis)
        return MuellerMat44(np.dot(MRp, self, MRn), dtype=dtype)

    @classmethod
    def create_absorber(cls, a, dtype=None):
        """Creates a Mueller absorber (attenuator) matrix

        ..  math::
            M_{A}(a) = \\begin{pmatrix}
                a   &   0   &   0   &   0 \\\\
                0   &   a   &   0   &   0 \\\\
                0   &   0   &   a   &   0 \\\\
                0   &   0   &   0   &   a
            \\end{pmatrix}

        Args:
            a (Number): transmission index from 0 to 1
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        return cls(a * np.eye(4), dtype=dtype)

    @classmethod
    def create_depol(cls, d=0, dtype=None):
        """Creates a Mueller matrix for a partial depolarizer.

        Multiplies the stokes coefficients :math:`(s_1,s_2,s_3)` by the factor
        :math:`0 \\le d \\le 1`. Set :math:`d=0` for an ideal depolarizer.

        ..  math::
            M_{U}(d) = \\begin{pmatrix}
                1   &   0   &   0   &   0 \\\\
                0   &   d   &   0   &   0 \\\\
                0   &   0   &   d   &   0 \\\\
                0   &   0   &   0   &   d
            \\end{pmatrix}

        Args:
            d (Number, optional):   Depolarization factor. Defaults to 0 for an ideal
                                    depolarizer
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        return cls([
            [1, 0, 0, 0],
            [0, d, 0, 0],
            [0, 0, d, 0],
            [0, 0, 0, d]
            ], dtype=dtype)

    @classmethod
    def create_h_polar(cls, dtype=None):
        """Creates a Mueller matrix for a perfect horizontal polarizer.

        ..  math::
            M_{Ph} = \\frac{1}{2} \\begin{pmatrix}
                1   &   1   &   0   &   0 \\\\
                1   &   1   &   0   &   0 \\\\
                0   &   0   &   0   &   0 \\\\
                0   &   0   &   0   &   0
            \\end{pmatrix}

        Args:
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        return cls(np.asarray([
            [1, 1, 0, 0],
            [1, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
            ]) / 2, dtype=dtype)

    @classmethod
    def create_v_polar(cls, dtype=None):
        """Creates a Mueller matrix for a perfect vertical polarizer

        ..  math::
            M_{Pv} = \\frac{1}{2} \\begin{pmatrix}
                 1  &   -1  &   0   &   0 \\\\
                -1  &    1  &   0   &   0 \\\\
                 0  &    0  &   0   &   0 \\\\
                 0  &    0  &   0   &   0
            \\end{pmatrix}

        Args:
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        return cls(np.asarray([
            [1, -1, 0, 0],
            [-1, 1, 0, 0],
            [0,  0, 0, 0],
            [0,  0, 0, 0]
            ]) / 2, dtype=dtype)

    @classmethod
    def create_h_diat(cls, q, r, dtype=None):
        """Creates a Mueller matrix for a horizontal linear diattenuator
        with maximum transmission of `q` and minimum of `r`

        ..  math::
            M_{Dh}(q,r) = \\frac{1}{2} \\begin{pmatrix}
                q+r     &   q-r     &   0               &  0 \\\\
                q-r     &   q+r     &   0               &  0 \\\\
                0       &   0       &   2\\sqrt{q r}    &  0 \\\\
                0       &   0       &   0               &  2\\sqrt{q r}
            \\end{pmatrix}

        Args:
            q (Number): maximum transmittance
            r (Number): minimum transmittance
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        twosqrtqr = 2*np.sqrt(q*r)
        return cls(np.asarray([
            [q + r, q - r,  0,          0],
            [q - r, q + r,  0,          0],
            [0,     0,      twosqrtqr,  0],
            [0,     0,      0,          twosqrtqr]
            ]) / 2, dtype=dtype)

    @classmethod
    def create_circ_diat(cls, q, r, dtype=None):
        """Creates a Mueller matrix for a circular diattenuator
        with maximum transmission of `q` and minimum of `r`

        ..  math::
            M_{Dc}(q,r) = \\frac{1}{2} \\begin{pmatrix}
                q+r   &  0             &  0             &  q-r \\\\
                0     &  2\\sqrt{q r}  &  0             &  0 \\\\
                0     &  0             &  2\\sqrt{q r}  &  0 \\\\
                q-r   &  0             &  0             &  q+r
            \\end{pmatrix}

        Args:
            q (Number): maximum transmittance
            r (Number): minimum transmittance
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        twosqrtqr = 2*np.sqrt(q*r)
        return cls(np.asarray([
            [q + r, 0,          0,          q - r],
            [0,     twosqrtqr,  0,          0],
            [0,     0,          twosqrtqr,  0],
            [q - r, 0,          0,          q + r]
            ]) / 2, dtype=dtype)

    @classmethod
    def create_partial_h_pol(cls, diat, dtype=None):
        """Creates a Mueller matrix for an imperfect horizontal linear polarizer
        with a given diattenuation.

        calls :py:meth:`create_h_diat` with arguments (q, r) derived from
        diattenuation D as

        ..  math::
            M_{Ph}(D) = M(q,r) \\quad \\text{where}\\quad \\begin{align}
                q &= \\tfrac{1+D}{2} \\\\
                r &= \\tfrac{1-D}{2}
            \\end{align}

        Args:
            diat (Number): diattenuation
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        q = (1 + diat)/2
        r = (1 - diat)/2
        return cls.create_h_diat(q, r, dtype=dtype)

    @classmethod
    def create_partial_circ_pol(cls, diat, dtype=None):
        """Creates a Mueller matrix for an imperfect circular polarizer
        with a given diattenuation.

        calls :py:meth:`create_circ_diat` with arguments (q, r) derived from
        diattenuation D as

        ..  math::
            M_{Pc}(D) = M(q,r) \\quad \\text{where}\\quad \\begin{align}
                q &= \\tfrac{1+D}{2} \\\\
                r &= \\tfrac{1-D}{2}
            \\end{align}

        Args:
            diat (Number): diattenuation
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        q = (1 + diat)/2
        r = (1 - diat)/2
        return cls.create_circ_diat(q, r, dtype=dtype)

    @classmethod
    def create_h_ret(cls, delta, dtype=None):
        """Creates a Mueller matrix for a linear retarder with a horizontal
        fast axis and retardance of :math:`\\delta`.

        ..  math::
            M_{\\Delta h}(\\delta) = \\begin{pmatrix}
                1   &   0   &   0               &   0 \\\\
                0   &   1   &   0               &   0 \\\\
                0   &   0   &   \\cos\\delta    &  \\sin\\delta \\\\
                0   &   0   &   -\\sin\\delta   &   \\cos\\delta
            \\end{pmatrix}

        Args:
            delta (Number): retardance in radians
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        cosdelta = np.cos(delta)
        sindelta = np.sin(delta)
        return cls([
            [1, 0,  0,          0],
            [0, 1,  0,          1],
            [0, 0,  cosdelta,   sindelta],
            [0, 0   -sindelta,  cosdelta]
            ], dtype=dtype)

    @classmethod
    def create_circ_ret(cls, delta, dtype=None):
        """Creates a Mueller matrix for a right-circular retarder with a
        retardance of :math:`\\delta`.

        ..  math::
            M_{\\Delta c}(\\delta) = \\begin{pmatrix}
                1   &   0               &   0               & 0 \\\\
                0   &   \\cos\\delta    &   \\sin\\delta    & 0 \\\\
                0   &   -\\sin\\delta   &   \\cos\\delta    & 0 \\\\
                0   &   0               &   0               & 1
            \\end{pmatrix}

        Args:
            delta (Number): retardance in radians
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        cosdelta = np.cos(delta)
        sindelta = np.sin(delta)
        return cls([
            [1, 0,          0,          0],
            [0, cosdelta,   sindelta,   0],
            [0, -sindelta,  cosdelta,   0],
            [0, 0,          0,          1]
            ], dtype=dtype)

    @classmethod
    def create_h_diatret(cls, q, r, delta, dtype=None):
        """Creates a Mueller matrix for a horizontal linear diattenuating
        retarder with maximum transmission of `q` and minimum of `r`, and
        and a horizontal fast axis with retardance :math:`\\delta`

        ..  math::
            M_{D\\Delta h}(q,r,\\delta) = \\frac{1}{2} \\begin{pmatrix}
                q+r     &   q-r     &   0                           &  0 \\\\
                q-r     &   q+r     &   0                           &  0 \\\\
                0       &   0       &   2\\sqrt{q r}\\cos\\delta    &  2\\sqrt{q r}\\sin\\delta \\\\
                0       &   0       &   -2\\sqrt{q r}\\sin\\delta   &  2\\sqrt{q r}\\cos\\delta
            \\end{pmatrix}

        Args:
            q (Number): maximum transmittance
            r (Number): minimum transmittance
            delta (Number): retardance in radians
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        twosqrtqr = 2*np.sqrt(q*r)
        cosdelta = np.cos(delta)
        sindelta = np.sin(delta)
        return cls(np.asarray([
            [q + r, q - r,  0,                      0],
            [q - r, q + r,  0,                      0],
            [0,     0,      twosqrtqr * cosdelta,   twosqrtqr * sindelta],
            [0,     0,      -twosqrtqr * sindelta,  twosqrtqr * cosdelta]
            ]) / 2, dtype=dtype)

    @classmethod
    def create_partial_h_polret(cls, diat, delta, dtype=None):
        """Creates a Mueller matrix for an imperfect horizontal linear polarizer
        with a given diattenuation and horizontal fast axis with retardance :math:`\\delta`.

        calls :py:meth:`create_h_diatret` with arguments (q, r, delta) derived from
        diattenuation D as

        ..  math::
            M_{D\\Delta h}(D,\\delta) = M(q,r,\\delta) \\quad \\text{where}\\quad \\begin{align}
                q &= \\tfrac{1+D}{2} \\\\
                r &= \\tfrac{1-D}{2}
            \\end{align}

        Args:
            diat (Number): diattenuation
            delta (Number): retardance in radians
            dtype (numpy.dtype, optional): Defaults to None. Data type of new array

        Returns:
            MuellerMat44: A 4x4 Mueller matrix
        """
        q = (1 + diat)/2
        r = (1 - diat)/2
        return cls.create_h_diatret(q, r, delta, dtype=dtype)

    # def __array_wrap__(self, out_arr, context=None):
    #     print('In __array_wrap__ context is {}:'.format(context))
    #     print('   self repr is {}'.format(np.array_repr(self, precision=2, suppress_small=True)))
    #     print('   arr repr is {}'.format(np.array_repr(out_arr, precision=2, suppress_small=True)))
    #     if len(self.shape) == 1 and self.shape[0] == 4:
    #         # vec = self.view(stokes.StokesVec4)
    #         vec = stokes.StokesVec4.__array_wrap__(self.view(stokes.StokesVec4), out_arr, context)
    #         # vec =  stokes.StokesVec4.__array_wrap__(self, out_arr, context)
    #         print('   found StokesVec. Returning as {}'.format(repr(vec)))
    #         return vec
    #     # then just call the parent
    #     print('   found Matrix shape {}'.format(self.shape))
    #     return sa.StaticArray.__array_wrap__(self, out_arr, context)

    # def __init__(self, *args, **kwargs):
    #     print('In __init__:')
    #     print('   self shape is {} repr is {}'.format(self.shape, np.array_repr(self, precision=2, suppress_small=True)))
    #     print('   args are ', args)
    #     print('   kwargs are ', kwargs)

    # def __array_finalize__(self, obj):
    #     print('In __array_finalize__:')
    #     print('   self shape is {} repr is {}'.format(self.shape, np.array_repr(self, precision=2, suppress_small=True)))
    #     print('   obj shape is {} repr is {}'.format(obj.shape, np.array_repr(obj, precision=2, suppress_small=True)))

    # def __array_prepare__(self, result, context=None):
    #     print('In __array_prepare__ context is {}:'.format(context))
    #     print('   self repr is {}'.format(np.array_repr(self, precision=2, suppress_small=True)))
    #     print('   result repr is {}'.format(np.array_repr(result, precision=2, suppress_small=True)))
    #     return result

    # def dot(self, b, out=None):
    #     print('In dot:')
    #     print('   self shape is {} repr is {}'.format(self.shape, np.array_repr(self, precision=2, suppress_small=True)))
    #     if not(out is None):
    #         print('   out shape is {} repr is {}'.format(out.shape, np.array_repr(out, precision=2, suppress_small=True)))
    #     res = super(MuellerMat44, self).dot(b, out=out)
    #     if len(res.shape) == 1 and res.shape[0] == 4:
    #         vec = res.view(stokes.StokesVec4)
    #         print('   res found StokesVec. Returning as {}'.format(repr(vec)))
    #         return vec
    #     print('   res shape is {} repr is {}'.format(res.shape, np.array_repr(res, precision=2, suppress_small=True)))
    #     return res