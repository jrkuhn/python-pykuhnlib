"""
Collection of example Stokes vectors along with their expected decompositions
"""
from collections import namedtuple
import numpy as np
from . import StokesVec4

StokesVecExample = namedtuple('ExampleStokesVec', [
    'vec', 'name', 'desc',
    'flux', 'dop', 'dolp', 'docp', 'ellipticity',
    'major_axis', 'minor_axis', 'major_angle',
    'ellipticity_angle', 'eccentricity', 'stokes_sphere_angles'
])
""":py:class:`namedtuple` class to hold a Stokes vector and its expected decompositions."""


SLH = StokesVecExample(
    vec=StokesVec4([1, 1, 0, 0]),
    name='SLH', desc="Linearly polarized (horizontal)",
    flux=1, dop=1, dolp=1, docp=0, ellipticity=0,
    major_axis=1, minor_axis=0, major_angle=0,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(0, 0)
)

SLV = StokesVecExample(
    vec=StokesVec4([1, -1, 0, 0]),
    name='SLV', desc="Linearly polarized (vertical)",
    flux=1, dop=1, dolp=1, docp=0, ellipticity=0,
    major_axis=1, minor_axis=0, major_angle=np.pi/2,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(np.pi, 0)
)

SL45 = StokesVecExample(
    vec=StokesVec4([1, 0, 1, 0]),
    name='SL45', desc="Linearly polarized (+45º)",
    flux=1, dop=1, dolp=1, docp=0, ellipticity=0,
    major_axis=1, minor_axis=0, major_angle=np.pi/4,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(np.pi/2, 0)
)

SL135 = StokesVecExample(
    vec=StokesVec4([1, 0, -1, 0]),
    name='SL135', desc="Linearly polarized (-45º)",
    flux=1, dop=1, dolp=1, docp=0, ellipticity=0,
    major_axis=1, minor_axis=0, major_angle=-np.pi/4,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(-np.pi/2, 0)
)

SCR = StokesVecExample(
    vec=StokesVec4([1, 0, 0, 1]),
    name='SCR', desc="Right-hand circularly polarized",
    flux=1, dop=1, dolp=0, docp=1, ellipticity=1,
    major_axis=1/np.sqrt(2), minor_axis=1/np.sqrt(2), major_angle=0,
    ellipticity_angle=np.pi/4, eccentricity=0,
    stokes_sphere_angles=(0, np.pi/2)
)

SCL = StokesVecExample(
    vec=StokesVec4([1, 0, 0, -1]),
    name='SCL', desc="Left-hand circularly polarized",
    flux=1, dop=1, dolp=0, docp=-1, ellipticity=-1,
    major_axis=1/np.sqrt(2), minor_axis=1/np.sqrt(2), major_angle=0,
    ellipticity_angle=-np.pi/4, eccentricity=0,
    stokes_sphere_angles=(0, -np.pi/2)
)

SUN = StokesVecExample(
    vec=StokesVec4([1, 0, 0, 0]),
    name='SUN', desc="Unpolarized",
    flux=1, dop=0, dolp=0, docp=0, ellipticity=0,
    major_axis=0, minor_axis=0, major_angle=0,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(0, 0)
)

SPAR = StokesVecExample(
    vec=StokesVec4([1, 0.5, 0.5, 0]),
    name='SPAR', desc="Partial Linearly polarized (22.5º)",
    flux=1, dop=1/np.sqrt(2), dolp=1/np.sqrt(2), docp=0, ellipticity=0,
    major_axis=1/(2**0.25), minor_axis=0, major_angle=np.pi/8,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(np.pi/4, 0)
)

SE45 = StokesVecExample(
    vec=StokesVec4([1, 0, 4.0/5, 3.0/5]),
    name='SE45', desc="Ellipically polarized (+45º)",
    flux=1, dop=1, dolp=4.0/5.0, docp=3.0/5.0, ellipticity=1.0/3.0,
    major_axis=3/np.sqrt(10), minor_axis=1/np.sqrt(10), major_angle=np.pi/4,
    ellipticity_angle=np.arctan2(1, 3), eccentricity=2*np.sqrt(2)/3,
    stokes_sphere_angles=(np.pi/2, 2*np.arctan2(1, 3))
)

SZERO = StokesVecExample(
    vec=StokesVec4([0, 0, 0, 0]),
    name='SZERO', desc="Empty",
    flux=0, dop=0, dolp=0, docp=0, ellipticity=0,
    major_axis=0, minor_axis=0, major_angle=0,
    ellipticity_angle=0, eccentricity=1,
    stokes_sphere_angles=(0, 0)
)

STOKES_EXAMPLES = [
    SLH, SLV, SL45, SL135,
    SCR, SCL, SUN, SPAR, SE45,
    SZERO
]
