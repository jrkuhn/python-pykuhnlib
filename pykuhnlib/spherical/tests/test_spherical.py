import unittest
from collections import namedtuple
import numpy as np
# from pprint import pprint, pformat
from pykuhnlib import spherical as sp

SphericalExample = namedtuple('SphericalExample', [
    'cart', 'name', 'spherical', 'poincare'
])

PI = np.pi
SQRT2 = np.sqrt(2)

ALL_EXAMPLES = [
    SphericalExample(
        name='+X',
        cart=(1, 0, 0),
        spherical=(1, PI/2, 0),
        poincare=(1, 0, 0)
    ),
    SphericalExample(
        name='-X',
        cart=(-1, 0, 0),
        spherical=(1, PI/2, PI),
        poincare=(1, 0, PI/2)
    ),
    SphericalExample(
        name='+Y',
        cart=(0, 1, 0),
        spherical=(1, PI/2, PI/2),
        poincare=(1, 0, PI/4)
    ),
    SphericalExample(
        name='-Y',
        cart=(0, -1, 0),
        spherical=(1, PI/2, -PI/2),
        poincare=(1, 0, -PI/4)
    ),
    SphericalExample(
        name='+Z',
        cart=(0, 0, 1),
        spherical=(1, 0, 0),
        poincare=(1, PI/4, 0)
    ),
    SphericalExample(
        name='-Z',
        cart=(0, 0, -1),
        spherical=(1, PI, 0),
        poincare=(1, -PI/4, 0)
    ),
    SphericalExample(
        name='El +X+Y',
        cart=(1/SQRT2, 1/SQRT2, 0),
        spherical=(1, PI/2, PI/4),
        poincare=(1, 0, PI/8)
    ),
    SphericalExample(
        name='El -X+Y',
        cart=(-1/SQRT2, 1/SQRT2, 0),
        spherical=(1, PI/2, 3*PI/4),
        poincare=(1, 0, 3*PI/8)
    ),
    SphericalExample(
        name='El -X-Y',
        cart=(-1/SQRT2, -1/SQRT2, 0),
        spherical=(1, PI/2, -3*PI/4),
        poincare=(1, 0, -3*PI/8)
    ),
    SphericalExample(
        name='El +Y+Z',
        cart=(0, 1/SQRT2, 1/SQRT2),
        spherical=(1, PI/4, PI/2),
        poincare=(1, PI/8, PI/4)
    ),
    SphericalExample(
        name='El +Y-Z',
        cart=(0, 1/SQRT2, -1/SQRT2),
        spherical=(1, 3*PI/4, PI/2),
        poincare=(1, -PI/8, PI/4)
    )
]

class TestSpherical(unittest.TestCase):

    def test_cart_to_spherical(self):
        for ex in ALL_EXAMPLES:
            spherical = sp.cart_to_spherical(*ex.cart)
            self.assertTrue(np.allclose(spherical, ex.spherical), msg=ex.name)

    def test_spherical_to_cart(self):
        for ex in ALL_EXAMPLES:
            cart = sp.spherical_to_cart(*ex.spherical)
            self.assertTrue(np.allclose(cart, ex.cart), msg=ex.name)

    def test_cart_to_poincare(self):
        for ex in ALL_EXAMPLES:
            poincare = sp.cart_to_poincare(*ex.cart)
            self.assertTrue(np.allclose(poincare, ex.poincare), msg=ex.name)

    def test_poincare_to_cart(self):
        for ex in ALL_EXAMPLES:
            cart = sp.poincare_to_cart(*ex.poincare)
            self.assertTrue(np.allclose(cart, ex.cart), msg=ex.name)

