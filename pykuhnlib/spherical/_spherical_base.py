"""
Spherical coordinates implementation file.
"""
import math
import numpy as np
#from numpy import linalg as LA
from pykuhnlib import staticarrays as sa

#==========================================================================================
# Spherical Coordinates functions
#==========================================================================================

def cart_to_spherical(x, y, z):
    """Convert cartesian to ISO spherical coordinates.

    Converts Cartesian coordinates :math:`(x,y,z)` to Spherical
    coordinates :math:`(r,\\theta_{inc},\\phi_{az})` using the IOS:80000-2:2009 standard.

    Args:
        x (Number): cartesian coordinates
        y (Number): cartesian coordinates
        z (Number): cartesian coordinates

    Returns:
        Tuple[Number]: a 3-element tuple containing

            -   **r** (Number) -- radial distance from center.

            -   **theta_inc** (Number) -- inclination angle from the z-axis.
                Ranges from 0 at the north pole to :math:`\\pi/2` at the equator to
                :math:`\\pi` at the south pole.

            -   **phi_az** (Number) -- azimuthal angle around the equator.
                Ranges from 0 (inclusive) at the +x-axis to :math:`\\pi/2` at the
                +y-axis to :math:`2\\pi` (exclusive) coming back around to the +x-axis.

    """
    r = np.sqrt(x*x + y*y + z*z)
    theta_inc = math.acos(z/r)
    phi_az = np.arctan2(y, x)
    return r, theta_inc, phi_az

def spherical_to_cart(r, theta_inc, phi_az):
    """Converts ISO spherical to cartesian coordinates

    Converts Spherical coordinates :math:`(r,\\theta_{inc},\\phi_{az})` to Cartesian
    coordinates :math:`(x,y,z)` using the IOS:80000-2:2009 standard.

    Args:
        r (Number): radial distance from center.
        theta_inc (Number): inclination angle from the z-axis.
            Ranges from 0 at the north pole to :math:`\\pi/2` at the equator to
            :math:`\\pi` at the south pole.
        phi_az (Number): azimuthal angle around the equator.
            Ranges from 0 (inclusive) at the +x-axis to :math:`\\pi/2` at the
            +y-axis to :math:`2\\pi` (exclusive) coming back around to the +x-axis.

    Returns:
        Tuple[Number]: a 3-element tuple containing the cartesian coordinates **(x, y, z)**
    """
    x = r * np.sin(theta_inc) * np.cos(phi_az)
    y = r * np.sin(theta_inc) * np.sin(phi_az)
    z = r * np.cos(theta_inc)
    return x, y, z

#==========================================================================================
# Poincare Coordinates functions
#==========================================================================================

def cart_to_poincare(x, y, z):
    """Converts cartesian coordinates to index ellipse angles

    Convert Cartesian coordinates :math:`(x,y,z)` to Poincare Sphere Angles
    :math:`(r, \\chi_{el}, \\psi_{az})`.

    Args:
        x (Number): cartesian coordinates
        y (Number): cartesian coordinates
        z (Number): cartesian coordinates

    Returns:
        Tuple[Number]: a 3-element tuple containing

            -   **r** (Number) -- radial distance from center.

            -   **chi_el** (Number) -- elliptical (elevation) angle of the index ellipse.

            -   **psi_az** (Number) -- azimuthal angle of the index ellipse.

    ..  note:: :math:`\\psi` (azimuth) represents the semi-major axis of the
        polarization ellipse in space and :math:`\\chi` (elevation) represents the
        ellipticity, so points on the Poincare Sphere map to :math:`(r,\\pi/2-2\\chi,2\\psi)`.

    """
    r, theta_inc, phi_az = cart_to_spherical(x, y, z)
    psi_az = phi_az/2
    chi_el = (np.pi/2 - theta_inc)/2
    return r, chi_el, psi_az

def poincare_to_cart(r, chi_el, psi_az):
    """ Convert index ellipse angles to cartesian coordinates

    Converts Poincare Sphere Angles :math:`(r,\\chi_{el},\\psi_{az})` to Cartesian
    coordinates :math:`(x,y,z)`.

    Args:
        r (Number): radial distance from center.
        chi_el (Number): elliptical (elevation) angle of the index ellipse.
        phi_az (Number): azimuthal angle of the index ellipse.

    Returns:
        Tuple[Number]: a 3-element tuple containing the cartesian coordinates **(x, y, z)**

    ..  note:: :math:`\\psi` (azimuth) represents the semi-major axis of the
        polarization ellipse in space and :math:`\\chi` (elevation) represents the
        ellipticity, so points on the Poincare Sphere map to :math:`(r,\\pi/2-2\\chi,2\\psi)`.
    """
    theta_inc = np.pi/2 - 2*chi_el
    phi_az = 2*psi_az
    return spherical_to_cart(r, theta_inc, phi_az)

#==========================================================================================
# Spherical Coordinates class
#==========================================================================================
class SphericalCoords(sa.StaticArray):
    """Spherical coordinates as a three-element ndarray

    Static 3-element array containing spherical coordinates

    Args:
        r (Number): radial distance from center.
        theta_inc (Number): inclination angle from the z-axis in radians.
        phi_az (Number): azimuthal angle around the equator in radians.

    """

    _shape = (3,)

    r = sa.IndexDescriptor(0)
    """Number: get/set descriptor for radial distance from center."""

    theta = sa.IndexDescriptor(1)
    """Number: get/set descriptor for inclination angle from the z-axis in radians.

    Ranges from 0 at the north pole to :math:`\\pi/2` at the equator to
    :math:`\\pi` at the south pole.
    """

    phi = sa.IndexDescriptor(2)
    """Number: get/set descriptor for azimuthal angle around the equator in radians.

    Ranges from 0 (inclusive) at the +x-axis to :math:`\\pi/2` at the
    +y-axis to :math:`2\\pi` (exclusive) coming back around to the +x-axis.
    """

    @classmethod
    def create_from_deg(cls, r, thetadeg, phideg):
        """Create spherical coordinates in radians from angles in degrees

        Args:
            r (Number): radial distance from center.
            thetadeg (Number): inclination angle from the z-axis in degrees.
            phideg (Number): azimuthal angle around the equator in degrees.

        Returns:
            SphericalCoords: A new 3-element array containing spherical coordinates
        """
        return cls([r, np.math.radians(thetadeg), np.math.radians(phideg)])

    @classmethod
    def create_from_cart(cls, x, y, z, dtype=None):
        """Create a SphericalCoords array from cartesian coordinates

        Args:
        x (Number): cartesian coordinates
        y (Number): cartesian coordinates
        z (Number): cartesian coordinates
        dtype (optional): Defaults to None. Element type defined by numpy

        Returns:
            SphericalCoords: A new 3-element array containing spherical coordinates
        """
        return cls([cart_to_spherical(x, y, z)], dtype=dtype)

    def as_cart_array(self):
        """Returns the cartesian coordinates corresponding to these spherical coordinates

        Returns:
            numpy.ndarray: (x,y,z) cartesian coordinates
        """
        return np.asarray([spherical_to_cart(self.r, self.theta, self.phi)])

#==========================================================================================
# Poincare Coordinates class
#==========================================================================================
class PoincareCoords(sa.StaticArray):
    """Poincare coordinates as a three-element ndarray

    Static 3-element array containing spherical coordinates

    Args:
        r (Number): radial distance from center.
        chi_el (Number): elliptical (elevation) angle of the index ellipse.
        phi_az (Number): azimuthal angle of the index ellipse.

    """
    _shape = (3,)
    r = sa.IndexDescriptor(0)
    """Number: get/set descriptor for radial distance from center."""

    chi = sa.IndexDescriptor(1)
    """Number: get/set descriptor for elliptical (elevation) angle of the index ellipse."""

    psi = sa.IndexDescriptor(2)
    """Number: get/set descriptor for azimuthal angle of the index ellipse."""


    @classmethod
    def create_from_deg(cls, r, chideg, psideg):
        """Create Poincare coordinates in radians from angles in degrees

        Args:
            r (Number): radial distance from center.
            chideg (Number): elliptical (elevation) angle of the index ellipse.
            phideg (Number): azimuthal angle of the index ellipse.

        Returns:
            PoincareCoords: A new 3-element array containing poincare coordinates
        """
        return cls([r, np.math.radians(chideg), np.math.radians(psideg)])

    @classmethod
    def create_from_cart(cls, x, y, z, dtype=None):
        """Create a PoincareCoords array from cartesian coordinates

        Args:
        x (Number): cartesian coordinates
        y (Number): cartesian coordinates
        z (Number): cartesian coordinates
        dtype (optional): Defaults to None. Element type defined by numpy

        Returns:
            PoincareCoords: A new 3-element array containing Poincare coordinates
        """
        return cls([cart_to_poincare(x, y, z)], dtype=dtype)

    def as_cart_array(self):
        """Returns the cartesian coordinates corresponding to these poincare coordinates

        Returns:
            numpy.ndarray: (x,y,z) cartesian coordinates
        """
        return np.asarray([poincare_to_cart(self.r, self.chi, self.psi)])
