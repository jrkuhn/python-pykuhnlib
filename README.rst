=========
PyKuhnLib
=========

A library of common Python polarization optics methods.

StaticArrays Module
-------------------

:py:mod:`pykuhnlib.staticarrays`

Fixed size helper classes for :py:mod:`numpy.ndarray`

Spherical Module
----------------
:py:mod:`pykuhnlib.spherical`

ISO Spherical and Poincare Spher coordinate conversions

Stokes Module
-------------
:py:mod:`pykuhnlib.stokes`

Stokes-Mueller Polarization calculus