.. _about_spherical_coords:

=====================
Spherical Coordinates
=====================

Spherical Coordinate Angles
---------------------------

There are a number of spherical coordinate conventions in prevalent use. Some use left-handed 
instead of right-handed coordinates, some reverse the order of the two angles, some swap 
the meanings of :math:`\phi` and :math:`\theta` (phi and theta), and some have different starting points 
for one of the angles. In any of these, we can refer to the angles by a few different names:

*Azimuth angle*
  Is typically the angle within the x-y plane, also the equator of the sphere. 
  It is almost always measured starting at the x-axis.

*Polar angle, altitude angle, zenith angle, elevation angle, inclination angle*
  Refer to an angle with respect to the z-axis. Altitude and elevation angles usually are 
  zero at the x-y plane, while polar, zenith, and inclination angles are usually zero along the z-axis.

ISO Standard
------------

ISO Standard 80000-2:2009 defines this convention. The greek letters are often substituted for 
their "curly" equivalents. So, :math:`\theta` (theta) is often used in its curly form of :math:`\vartheta`
and :math:`\phi` (phi) is shown in its curly form of :math:`\varphi`.
*We will primarily use the non-curly forms of theta and phi below.*

Physics/ISO Spherical Angle Convention
--------------------------------------

.. figure:: resources/iso_spherical_coords.png
    :scale: 50 %
    :align: center

    IOS:80000-2:2009 standard spherical coordinates

    NOTE: The greek angles theta and phi are shown in their curly forms of :math:`\vartheta`
    and :math:`\varphi`.

The primary convention in physics is 
:math:`(r_\text{radius}, \theta_\text{inclination angle}, \phi_\text{azimuth angle})` 
in a right-handed coordinate system. Here the inclination angle :math:`\theta` starts at 
the z-axis :math:`(\cos\theta=0)`. The corresponding transformation from spherical to 
cartesion coordinates is:

Forward ISO Standard conversion from spherical coordiantes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. math::
    (r,\theta _{inc},\phi _{az}) \overset{Fwd}{\to} (x,y,z) 
    = (r \cos\phi \sin\theta, r \sin\phi \sin\theta,r \cos\theta)

where

.. math::
	r       &\in [0,\infty) \\
    \theta &\in [0,\pi] \\
    \phi   &\in [0,2\pi)
	
Reverse ISO Standard conversion to spherical coordiantes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. math::

    (x,y,z) \overset{Rev}{\to} (r,\theta,\phi)
    = \left(
          \sqrt{x^2+y^2+z^2}, 
          \cos^{-1}\left[\frac{z}{\sqrt{x^2+y^2+z^2}}\right],
          \tan^{-1}\left[\frac{y}{x}\right] 
        \right)


.. note:: 
    **Degeneracy**
        Both x- and y-coordinates are zero along the z-axis. 
        This makes spherical azimuth angles :math:`\phi_{az}` indeterminate along the 
        z-axis because there is no :math:`\tan^{-1}(0/0)`. Spherical coordinate transformations 
        to/from cartesian coordinates are technically degenerate in this case. 
        
        The spherical Coordinate transformation in this module just uses :math:`\phi_{az}=0`
        along the z-axis. The ``numpy.arctan2(0,0)`` function returns ``0`` in this case,
        so degeneracy is handled automatically.

Poincare Sphere Angle Convention
--------------------------------

.. figure:: resources/poincare_coords.png
   :scale: 50 %
   :align: center

   Polarization coordinates on the Poincare sphere and index ellipse

The Poincare sphere is often depicted as using azimuth and elevation angles. The azimuth 
angle is usually doubled to reflect the physical doubled orientation angle of fast-axis 
orientation within the x-y axis. One full circle around the equator of the Poincare 
sphere represents 180º of rotation of the physical fast-axis of the polarizing device 
with respect to the optical axis. To keep with this convention, the elevation angle 
is also doubled to represent the doubling of the ellipticity angle of the polarization 
ellipse. To reduce confusion, the greek letters :math:`\psi` and :math:`\chi` (psi and chi) 
are often used to represent a ngles instead of :math:`\phi` and :math:`\theta` (phi and theta). 

Forward conversion from Poincare spherical coordiantes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. math::
    (r,2\psi_\text{az},2\chi_\text{el}) \overset{Fwd}{\to}
     (r \cos 2\psi \cos 2\chi, r \sin 2\psi \cos 2\chi, r \sin 2\chi)
