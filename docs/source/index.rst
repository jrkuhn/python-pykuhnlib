.. PyKuhnLib documentation master file, created by
   sphinx-quickstart on Fri Feb  2 17:05:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   about_spherical_coords
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. include:: ../../README.rst
