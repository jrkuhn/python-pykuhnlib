from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='pykuhnlib',
      version='0.1',
      description='Collection of python libraries',
      author='Jeffrey R Kuhn',
      author_email='drjrkuhn@gmail.com',
      license='MIT',
      packages=['pykuhnlib'],
      install_requires=[
          'numpy',
      ],
      zip_safe=False)
